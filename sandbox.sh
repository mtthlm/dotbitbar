#!/usr/bin/env bash

function __element () {
  local __format='%s'
  local -a __params=("${1}"); shift

  if [[ ${#} -gt 0 ]]; then
    local -a __sub_formats=()

    while [[ ${#} -gt 0 && "${1}" == *'='* ]]; do
      __sub_formats+=('%s=%q')
      __params+=("${1%%=*}" "${1#*=}"); shift
    done

    __format="${__format}|${__sub_formats[@]}"
  fi

  printf "${__format}\n" "${__params[@]}"
}

__element 'Hello, world!' bash='hello world' param1='blah'
__element 'Blah!'
