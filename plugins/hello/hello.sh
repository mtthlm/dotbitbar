#!/usr/local/bin/bash
function __realdir () (cd -P "${1}" &> /dev/null && pwd)
function __dirname () (__realdir "$(dirname "${1}")")

__self="${BASH_SOURCE[0]}"

while [[ -h "${__self}" ]]; do
  __dir="$(__dirname "${__self}")"
  __self="$(readlink "${__self}")"
  [[ "${__self}" == '/'* ]] || __self="${__dir}/${__self}"
done

declare -r __dir="$(__dirname "${__self}")"
declare -r __file="${__dir}/$(basename "${__self}")"

unset -v __self

source "${__dir}/../plugin.sh"

# ----

echo 'Hello, world!'
