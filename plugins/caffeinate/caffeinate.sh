#!/usr/local/bin/bash
function __realdir () (cd -P "${1}" &> /dev/null && pwd)
function __dirname () (__realdir "$(dirname "${1}")")

__self="${BASH_SOURCE[0]}"

while [[ -h "${__self}" ]]; do
  __dir="$(__dirname "${__self}")"
  __self="$(readlink "${__self}")"
  [[ "${__self}" == '/'* ]] || __self="${__dir}/${__self}"
done

declare -r __dir="$(__dirname "${__self}")"
declare -r __file="${__dir}/$(basename "${__self}")"

unset -v __self

source "${__dir}/../plugin.sh"

# ----

# declare -r __lockfile="${__dir}/.lockfile"

# function __locked () (test -f "${__lockfile}")
# function __lock () (touch "${__lockfile}")
# function __unlock () (rm -f "${__lockfile}")
# function __pid () (cat "${__lockfile}")

# __locked && echo "PID: $(__pid)" || echo 'Decaffeinated'

# exit

function __coffee_emoji () (printf '\xe2\x98\x95\xef\xb8\x8f')

case "${1:-}" in
  'caffeinate')
    killall caffeinate &>/dev/null || true
    caffeinate -d </dev/null &>/dev/null &
    exit
    ;;
  'decaffeinate')
    killall caffeinate &>/dev/null || true
    exit
    ;;
esac

if __pid="$(pgrep -x caffeinate)"; then
  __elapsed="$(ps -o 'etime=' -p "${__pid}")"

  __element "$(__coffee_emoji) ${__elapsed}"
  __element '---'
  __element 'Decaffeinate' bash="${__file}" param1='decaffeinate' terminal=false refresh=true
else
  __element "$(__coffee_emoji)"
  __element '---'
  __element 'Caffeinate' bash="${__file}" param1='caffeinate' terminal=false refresh=true
fi
