set -o errexit
set -o errtrace
set -o nounset
set -o pipefail

function __join_by () (IFS="${1}"; shift; echo "${*}")

function __element () {
  local __format='%s'
  local -a __params=("${1}"); shift

  if [[ ${#} -gt 0 ]]; then
    local -a __sub_formats=()

    while [[ ${#} -gt 0 ]]; do
      if [[ "${1}" == *'='* ]]; then
        __sub_formats+=('%s=%q')
        __params+=("${1%%=*}" "${1#*=}")
      fi

      shift
    done

    __format="${__format}|${__sub_formats[@]}"
  fi

  printf "${__format}\n" "${__params[@]}"
}
